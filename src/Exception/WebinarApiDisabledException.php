<?php

namespace Procontext\WebinarApi\Exception;

use Throwable;

class WebinarApiDisabledException extends WebinarApiException
{
    public function __construct($message = "Webinar API отключен", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}